import argparse
import locale

import xlsxwriter
import yaml

locale.setlocale(locale.LC_ALL, "it_IT")

parser = argparse.ArgumentParser()
parser.add_argument("weekreport", type=argparse.FileType("r", encoding="utf8"))
args = parser.parse_args()

# LOAD WEEK DATA

with args.weekreport as weekreport:
    week = yaml.safe_load(weekreport.read())


# Create a workbook and add a worksheet.
xlsx_filename = f"rendicontazione smart working Costa Stefano {args.weekreport.name.replace('yml', 'xlsx')}"
workbook = xlsxwriter.Workbook(xlsx_filename)
worksheet = workbook.add_worksheet()

# CELL FORMATS

bold = workbook.add_format({"bold": True})

header_format = workbook.add_format({"bold": True, "text_wrap": True, "align": "top"})
subheader_format = workbook.add_format({"bold": True, "align": "center"})
sec_title_format = workbook.add_format({"bold": True, "align": "left"})
sec1_body_format = workbook.add_format({"align": "justify", "text_wrap": True})
sec1_body_format.set_align("top")
sec4_header_format = workbook.add_format(
    {"bold": True, "align": "vcenter", "text_wrap": True}
)
sec4_header_format.set_align("center")
sec4_body_format = workbook.add_format({"align": "vcenter", "text_wrap": True})

# WORKSHEET HEADER

worksheet.set_row(0, 45)
title = "Schema di relazione sull'attività svolta, contenente una valutazione complessiva dei risultati conseguiti in termini di obiettivi raggiunti nel periodo considerato e/o la misurazione della produttività delle attività svolte complessivamente"
worksheet.merge_range("A1:K1", title, header_format)

date_range = f"Periodo di riferimento {week['date_start']:%d %B %Y} - {week['date_end']:%d %B %Y}"
worksheet.merge_range("A3:K3", date_range, subheader_format)

worksheet.write("A5", "DIPENDENTE: Stefano Costa")
worksheet.write("A6", "QUALIFICA: Archeologo AREA III F1")

activities = {
    "Espletamento istruttorie connesse alla tutela del Patrimonio Archeologico": {
        "descrizione": "consultazione di GIADA per visione delle pratiche assegnate alle diverse UO a me pertinenti (UFFICIO CATALOGO; UT IMPERIA OVEST; UT IMPERIA EST);  verifica delle pratiche da prendere in carico in quanto di competenza archeologica o, anche, di competenza mista; classificazione e fascicolazione di quelle di competenza; istruttoria in dialogo con le altre competenze eventualmente coinvolte (architetti); predisposizione degli atti finali.",
        "indicatore": [
            "indicatore di risultato",
            "numero procedimenti istruiti",
            "tutti quelli previsti ",
        ],
        "valore": "{totale} protocolli visionati, {competenza} protocolli selezionati per seguito di competenza, {firma} atti posti alla firma",
    },
    "Direzione e coordinamento cantieri di scavo/assistenza archeologica": {
        "descrizione": "direzione tecnico-scientifica e operativa dei cantieri di diretta competenza dell'ufficio e di quelli operanti sotto sorveglianza",
        "indicatore": [
            "indicatore di attività",
            "contatti con direzione lavori / professionisti",
            "almeno due contatti / settimana",
        ],
        "valore": "{contatti}",
    },
    "Direzione attività catalogazione e inventariazione": {
        "descrizione": "Direzione attività catalogazione e inventariazione",
        "indicatore": [
            "indicatore di attività",
            "contatti con collaboratori",
            "almeno un contatto / giorno",
        ],
        "valore": "{contatti}",
    },
    "Attività connesse all'incarico di RUP": {
        "descrizione": "attività amministrativa di predisposizione atti e documenti per lavori pubblici",
        "indicatore": [
            "indicatore di risultato",
            "numero atti / documenti predisposti",
            "tutti quelli previsti",
        ],
        "valore": "{atti}",
    },
    "Attività istruttoria per procedimenti di verifica dell'interesse culturale": {
        "descrizione": "attività istruttoria tecnica endoprocedimentale per i procedimenti di verifica dell'interesse culturale relativi al patrimonio archeologico",
        "indicatore": [
            "indicatore di risultato",
            "numero istruttorie completate",
            "tutte quelle previste",
        ],
        "valore": "{istruttorie} svolte, di cui {positive} con esito positivo",
    },
    "Redazione contributi per Archeologia in Liguria": {
        "descrizione": "attività editoriale di revisione e redazione contributi per la pubblicazione della rivista ”Archeologia in Liguria”",
        "indicatore": [
            "indicatore di risultato",
            "numero contributi completati",
            "tutti quelli previsti",
        ],
        "valore": "{contributi} completati",
    },
       "Formazione e aggiornamento": {
        "descrizione": "attività di formazione e aggiornamento professionale",
        "indicatore": [
            "indicatore di attività",
            "numero lezioni seguite",
            "almeno due lezioni / settimana",
        ],
        "valore": "{lezioni}",
    },
}

for a in week["activities"]:
    a["general"] = activities[a["title"]]
    a["valore"] = a["general"]["valore"].format(**a["indicatore"])
wkas = week["activities"]

# section 1

sec1_title = "DESCRIZIONE SINTETICA DELLE ATTIVITÀ SVOLTE"
worksheet.merge_range("A8:K8", sec1_title, sec_title_format)

activities_summary = "".join(
    f"{n+1}) {activity['title']}: {activity['general']['descrizione']} " for n, activity in enumerate(wkas)
)
sec1_body = "Attività ordinaria: " + activities_summary + week["note"]
worksheet.merge_range("A9:K15", sec1_body, sec1_body_format)

# section 2

sec2_title = "LINEE DI ATTIVITÀ SVOLTE DALL'UFFICIO:"
worksheet.merge_range("A17:K17", sec2_title, sec_title_format)

row = 17
col = 0

# Iterate over the data and write it out row by row.
for n, activity in enumerate(wkas):
    worksheet.write(row, col, f"{n + 1})")
    worksheet.write(row, col + 1, activity["title"])
    row += 1
worksheet.write(row, col, f"n. {len(wkas)}")

row += 2  # leave one blank row

# section 3

sec3_title = "INDICATORI (*)"
worksheet.merge_range(row, col, row, col + 10, sec3_title, sec_title_format)
row += 1

worksheet.merge_range(row, 0, row, 2, "TIPO DI INDICATORE", bold)
worksheet.merge_range(row, 3, row, 6, "DESCRIZIONE INDICATORE", bold)
worksheet.merge_range(row, 7, row, 10, "VALORE TARGET(**)", bold)

row += 1

for n, activity in enumerate(wkas):
    worksheet.merge_range(
        row, 0, row, 2, f'{n+1}) {activity["general"]["indicatore"][0]}'
    )
    worksheet.merge_range(row, 3, row, 6, activity["general"]["indicatore"][1])
    worksheet.merge_range(row, 7, row, 10, activity["general"]["indicatore"][2])
    row += 1

worksheet.set_row(row, 45, sec1_body_format)
sec3_comment1 = "(*) L’indicatore sarà scelto in base alla tipologia e alle caratteristiche delle attività lavorative svolte (es. indicatori di avanzamento di attività/progetti; indicatori di risultato)."
worksheet.merge_range(row, 0, row, 10, sec3_comment1)
row += 1
sec3_comment2 = "(**) Definire il livello di risultato ottenuto in termini numerici."
worksheet.merge_range(row, 0, row, 10, sec3_comment2)
row += 2  # leave one blank row

# section 4

worksheet.set_row(row, 45)
worksheet.merge_range(
    row, 0, row, 5, "OBIETTIVI/RISULTATI CONSEGUITI", sec4_header_format
)
worksheet.merge_range(
    row, 6, row, 7, "AVANZAMENTO RAGGIUNTO", sec4_header_format,
)
worksheet.merge_range(row, 8, row, 10, "NOTE/CRITICITÀ", sec4_header_format)
row += 1

for n, activity in enumerate(wkas):
    worksheet.set_row(row, 80, sec4_body_format)
    worksheet.merge_range(
        row, 0, row, 5, f"{n+1}) {activity['general']['indicatore'][1]}"
    )
    worksheet.merge_range(row, 6, row, 7, activity["valore"])
    worksheet.merge_range(row, 8, row, 10, activity["indicatore"]["note"])
    row += 1

workbook.close()
